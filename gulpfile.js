// iniciacion de modulos
const {src, dest, watch, series, parallel} = require('gulp');
const gulp = require('gulp');
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano');
const concat = require('gulp-concat');
const postcss = require('gulp-postcss');
const replace = require('gulp-replace');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');


// ruta de los archivos
const files = {
    scssRuta: '/css/*.scss',
    jsRuta: '/js/*.js'
}

//Tarea Sass
function scssTask(){
    return src(files.scssRuta)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('dist')
    );
}

//Tarea js
function jsTask(){
    return src(files.jsRuta)
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(dest('dist')
    );
}

// Tarea cahce
const cbString = new Date().getTime();
function cacheBustTask(){
    return src(['index.html'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(dest('.')
    );
}

//Tarea task
function watchTask(){
    watch([files.scssRuta, files.jsRuta], 
        series(
            parallel(scssTask, jsTask),
            cacheBustTask
        )
    );    
}

//browser-sync

function browserTask(){
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
}




//Deafault
exports.default = series(
    parallel(scssTask, jsTask),
    cacheBustTask,
    parallel(browserTask,watchTask)
    
);






